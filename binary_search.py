# Binary Search
# From "Discrete Mathematics and it's Applications" by Rosen

#    i                     m                           j      
A = [1, 8, 9, 12, 14, 15, 17, 20, 23, 24, 25, 28, 30, 31]
#                              m
#                              +
#                              1

def binary_search(x,A):
    n = len(A)
    i = 0
    j = n

    while i < j:
        m = (i + j)/2
        if x > A[m]:
            i = m + 1
        else:
            j = m
    if x == A[i]:
        location = i
    else:
        location = None
    return location

n = binary_search(20,A)
print n
