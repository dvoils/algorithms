from random import shuffle
import math
import numpy as np

# From "Introduction to Algorithms"

# Insert sort
#
def insertSort(a):
    n = len(a)

    # range(2,n) when counting from 1
    for j in range(1,n):
        key = a[j]
        i = j - 1
        
        # i > 0 when counting from 1
        while i>-1 and a[i] > key:
            a[i+1] = a[i]
            i = i - 1

        a[i+1] = key
    return a

# Quick sort recursive
#   a is array to sort
#   p is sort start point
#   r is sort end point
#
def swap(a,b):
    temp = a
    a = b
    b = temp
    return a,b

def partition(a,p,r):
    x = a[r]
    i = p - 1
    for j in range(p,r):
        if a[j] <= x:
            i = i + 1
            a[i],a[j] = swap(a[i],a[j])
    a[i + 1],a[r] = swap(a[i + 1],a[r])
    return i+1

def quickSort(a,p,r):
    if p < r:
        q = partition(a,p,r)
        quickSort(a,p,q-1)
        quickSort(a,q+1,r)
    return a

# merge sort recursive
#    A is array to sort
#    p is sort start point
#    r is sort end point
def merge(A,p,q,r):
    n1 = q - p + 1
    n2 = r - q

    L = np.zeros(n1+1)
    R = np.zeros(n2+1)

    L[n1] = np.inf
    R[n2] = np.inf
    
    for i in range(0,n1):
        L[i] = A[p + i]
    for i in range(0,n2):
        R[i] = A[q + i + 1]

    i = 0
    j = 0
    for k in range(p,r+1):

        if L[i] <= R[j]:
            A[k] = L[i]
            i = i + 1
        else:
            A[k] = R[j]
            j = j + 1
    return A

def mergeSort(A,p,r):

    if p < r:
        q = (p+r)//2

        A = mergeSort(a,p,q)
        A = mergeSort(a,q+1,r)
        A = merge(a,p,q,r)
   
        return A

N = 20
a = np.random.permutation(N)
b = a.copy()
c = a.copy()

print(a)
a = insertSort(a)
b = mergeSort(b,0,N-1)
c = quickSort(c,0,N-1)

if (a==b).all() and (b==c).all():
    print(c)
else:
    print('fail')



