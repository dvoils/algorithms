import random

class Node:
    def __init__(self, nodeID, adj, value):
        self.name = nodeID
        self.adj = adj
        self.value = value

# Breadth First Search
# input: starting point, graph list
#
def BFS(n,G):
    s = G[n]
    level = {s : 0}
    parent = {s : None}
    i = 1
    frontier = [s]
    while frontier:
        next = []
        for u in frontier:
            for v in u.adj:
                if v not in level:
                    level[v] = i
                    parent[v] = u
                    next.append(G[v])
                    print G[v].value
        frontier = next
        i = i + 1

# Create a random graph
#
N = 20
numVal = 100
nAdj = 4
nodeIDs = range(0,N)
nodes = []

for i in range(0,N):
    numAdj = random.randint(1,nAdj)
    adj = random.sample(nodeIDs, numAdj)
    nodeValue = random.randint(0,numVal)
    mynode = Node(i, adj, nodeValue)
    nodes.append(mynode)

# Search the Graph.
# Start at an arbitrary point
#
BFS(3,nodes)
