# Some notes on Algorithms and Discrete Math

## Sorting
### Insert Sort
Sorts by segmenting items into a sorted section and a non-sorted section. 
Items are moved one-by-one from the unsorted section to it's place in the sorted section.
+ Simple
+ A good choice if items are close to being sorted already
+ Good for on-line sorting

#### Analysis
The complexity of insertion sort has to do with inserting a unsorted item into the sorted part. 
To examine the worst case, let c be the time it takes to traverse every element of the sorted array.
Let n be the number of elements to be sorted.
The time t used by the algorithm is:

```math
t = \sum_{k=1}^{n-1} c + k
```

This is an arithmetic progression with d=1, so

```math
t = \frac{n (2c + n - 1 )}{2}
```

The complexity then is

```math
\Theta(n^2)
```
### Quick Sort

The quick sort algorithm was developed in 1959 by the British computer scientist sir Tony Hoare.
It is a divide and conquer algorithm that does the following:
1. Pick an element in the array, this is the *pivot*
2. Arrange elements such that those greater than the pivot go after and those less than the pivot go before
3. Apply steps 1 and 2 recursively to each partition created in step 2
4. Stop at the base case where each partition contains only one element

### Python Examples
[Sorting Examples](sort.py)

## Fibonacci

+ Every third number is even

```math
E(n)=4E(n-1)-E(n-2)
```

+ Equivalently

```math
F(n)=4F(n-3)+F(n-6)
```
+ [Fibonacci examples](fibbonacci.py)


## Integers
+ Slicing - Given an integer N, the rightmost m numbers

```math
M = N \bmod 10^m
```

+ Sum of integers 1 to N

```math
N = \frac{n (n + 1)}{2}
```


## Prime Number

### Some useful facts:
+ 1 is not a prime
+ All primes except 2 are odd
+ All primes greater than 3 can be written in the form 6k+/-1.
+ Any number n can have only one prime factor greater than n .
+ If we cannot find a number f less than or equal n that divides n then n is prime

### Sieve of Eratosthenes
An ancient algorithm for finding all the prime numbers less than some number
+ [Sieve of Eratosthenes example](sieve-of-eratosthenes.py)


## Factors
### Examples showing fast algorithms to find find factors of a number
+ [Find all prime factors](prime-factors.py)
+ [Find all factors](factors.py)

### Finding all factors using primes
Using the fundamental theorem of arithmetic

```math
N = p_1^{q_1} p_2^{q_2} ... p_m^{q_m}
```

Find factors of a number by enumerating all combinations of prime factors


+ [Example showing how to find all factors using primes](factors-from-primes.py)


## Binary search tree
The nodes of a BST have three edges, one points to the parent node and two that point to left and right children nodes.
+ [bst.py](bst/bst.py)
+ [test.py](bst/test.py)

![bst](bst/bst.png)

### Deleting
There are three cases.
The variable y represents the node to be deleted and is shown by the red node.
The variable x represents the pruned section of the tree that is attached after the node is deleted and is shown by the yellow node.

#### One Child

![delete one](bst/delete-left-one-child.png)

#### Two Children
![delete two](bst/delete-two-children.png)
