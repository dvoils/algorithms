# Recursive
#
def fib1(n):
    global i
    i = i + 1
    if n > 1:
        a = fib1(n-1)
        b = fib1(n-2)
        return a + b
    else:
        return n

# Memoized recursive
#
def fib2(n):
    global i
    i = i + 1
    if n in m:
        return m[n]
    if n<=2: 
        f = 1
    else:
        f = fib2(n-1) + fib2(n-2)
    m[n] = f
    return f

# Bottom up, non-recursive
#
def fib3(n):
    m = []
    m.append(1)
    m.append(1)
    for k in range(2, n):
        m.append(m[k-1] + m[k-2])
    return m

N = 10
m = {}

i = 0
a = fib1(N)
print "recursive ",i,a

i = 0
a = fib2(N)
print "memoized ",i,a

m = fib3(N)
print "bottom up ", m

