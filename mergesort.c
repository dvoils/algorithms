#include <stdio.h>

#define SIZE 20

void merge(int * A, int p, int q, int r){
   int n1 = q - p + 1;
   int n2 = r - q; 
   int i, j, k;
   int L[n1+1];
   int R[n2+1];

   L[n1] = 123456798;
   R[n2] = 123456798;

   for(i = 0; i < n1; i++)
     L[i] = A[p + i];
   for(j = 0; j < n2; j++)
     R[j] = A[q + j + 1];

   i = 0;
   j = 0;

   for(k = p; k <= r; k++){
     if(L[i] <= R[j]){
       A[k] = L[i];
       i++;
     } else {
       A[k] = R[j];
       j++;
     }
   }
 }

void merge_sort(int * A, int p, int r){
   if(p < r){
     int q = (p+r)/2;
     merge_sort(A, p, q);
     merge_sort(A, q + 1, r);
     merge(A, p, q, r);
   }
 }

void print_array(int * A, int amout_of_integers){
  int i;
  for(i = 0; i < amout_of_integers; i++)
    printf("%d ",A[i]);
  puts("");
}

int main(void){
//  int dataset[] = {2, 4, 5, 7, 1, 2, 3, 6};
int dataset[] = { 6,16,13,10, 1, 4,17, 8, 0, 7,18,15, 5, 3,19,12, 2,14, 9,11};

  print_array(dataset, SIZE);
  merge_sort(dataset, 0, SIZE-1);
  print_array(dataset, SIZE);

  return 0;
}
