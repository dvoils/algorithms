import math

# Sieve of Eratosthenes
#
def sieveOfEratosthenes(n):
    A = [True] * n

    if n < 2:
        print "error: n must be greater than 1"

    for i in range(2, int(math.sqrt(n))):
        if A[i] == True:
            k = i**2
            for j in range(1, n):
                #print i, k
                A[k] = False
                k = i**2 + j*i
                if k > n-1:
                    break

    A[2] = False

    j = 0
    for i in range(0,n):
        if A[i] == True:
            a = 1
            print j, i
            j = j + 1

sieveOfEratosthenes(10**6)
