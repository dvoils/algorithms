# Binary search tree example
# From Introduction to Algorithms text book
#

class Node:
    def __init__(self, key, p, left, right):
        self.key = key
        self.p = p  #predecesor node
        self.left = left
        self.right = right

class BinarySearchTree:
    def __init__(self, A):
        self.T = None
        for i in range(0,len(A)):
            z = Node(A[i], None, None, None)
            self.T = self.insert(self.T, z)

    # Insert new node into the tree
    # r = root node
    # z = node to insert
    # x = current node
    # y = previous node
    def insert(self,r,z):
        y = None
        x = r
        while x != None:
            y = x
            if z.key < x.key:
                x = x.left
            else:
                x = x.right
        z.p = y # set predecessor node
        if y == None: # Empty tree case when first building tree
            r = z
        elif z.key < y.key:
            y.left = z
        else:
            y.right = z
        return r

    # Search tree for a key value
    # Using iteration
    #
    def search(self,k):
        x = self.T
        while x != None and k != x.key:
            if k < x.key:
                print x.key, "left"
                x = x.left
            else:
                print x.key, "right"
                x = x.right
        
        return x

    # Finds the minimum key in the tree
    #
    def minimum(self,x):
        while x.left != None:
            x = x.left
        return x

    def successor(self,x):
        if x.right != None:
            return self.minimum(x.right)
        y = x.p
        while y != None and x == y.right:
            x = y
            y = y.p
        return y

    def delete(self,z):
        if z.left == None or z.right == None:
            # testDeleteLeftOneChild, testDeleteLeftNoChildren, testDeleteRightNoChildren, testDeleteRoot
            y = z
        else:
            # testDeleteLeftNodeTwoChildren
            y = self.successor(z)

        if y.left != None:
            # testDeleteLeftOneChild
            x = y.left
        else:
            # testDeleteLeftNodeTwoChildren, testDeleteLeftNoChildren, testDeleteRightNoChildren, testDeleteRoot
            x = y.right

        if x != None:
            #testDeleteLeftOneChild, testDeleteLeftNodeTwoChildren
            x.p = y.p
        
        if y.p == None:
            # testDeleteRoot
            T = x
        elif y == y.p.left:
            # testDeleteLeftOneChild, testDeleteLeftNodeTwoChildren, testDeleteLeftNoChildren
            y.p.left = x
        else:
            # testDeleteRightNoChildren
            y.p.right = x

        if y != z:
            # testDeleteLeftNodeTwoChildren
            z.key = y.key






