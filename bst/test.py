import unittest
from bst import BinarySearchTree

class unitTest(unittest.TestCase):

    # Delete left node that has no children
    #
    def testDeleteLeftNoChildren(self):
        A = [15,5,16,3,12,20,10,13,18,23,6,7]
        bst = BinarySearchTree(A)
        z = bst.search(3)
        bst.delete(z)
        self.assertEqual(bst.T.left.left, None)
 
    # Delete right node that has no children
    #
    def testDeleteRightNoChildren(self):
        print "testDeleteRightNoChildren"
        A = [15,5,16,3,12,20,10,13,18,23,6,7]
        bst = BinarySearchTree(A)
        z = bst.search(13)
        bst.delete(z)
        self.assertEqual(bst.T.left.right.right, None)

    # Tree contains one node
    #
    def testDeleteRoot(self):
        print "testDeleteRoot"
        A = [15]
        bst = BinarySearchTree(A)
        z = bst.search(15)
        y = bst.delete(z)
        self.assertEqual(y, None)
    
    # Delete left node that has two children
    #
    def testDeleteLeftNodeTwoChildren(self):
        print "testDeleteLeftNodeTwoChildren"
        A = [15,5,16,3,12,20,10,13,18,23,6,7]
        bst = BinarySearchTree(A)
        z = bst.search(5)
        bst.delete(z)
        self.assertEqual(bst.T.left.key, 6)
        self.assertEqual(bst.T.left.right.left.left.key, 7)
    
    # Delete left node that has one child
    #
    def testDeleteLeftOneChild(self):
        print "testDeleteLeftOneChild"
        A = [15,5,16,3,12,20,10,13,18,23,6,7]
        bst = BinarySearchTree(A)
        z = bst.search(10)
        bst.delete(z)
        self.assertEqual(bst.T.left.right.left.key, 6)


if __name__ == '__main__':
    unittest.main()