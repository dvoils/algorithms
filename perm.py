# Permutate a list

# From Kris Write YouTube tutorial
# Returns an iterator
#
def perm1(lst):
    if len(lst) == 0:
        yield []
    elif len(lst) == 1:
        yield lst
    else:
        for i in range(len(lst)):
            x = lst[i]
            xs = lst[:i] + lst[i + 1:]
            for p in perm(xs):
                yield [x] + p

data = list('abcd')

# From GeeksForGeeks
#
def perm2(a, l, r):
    if l==r:
        print a
    else:
        for i in xrange(l,r+1):
            a[l], a[i] = a[i], a[l]
            perm2(a, l+1, r)
            a[l], a[i] = a[i], a[l]
   
        


#a = perm2(data)
n = len(data)
q = perm2(data, 0, n-1)

print q
#print(a[20])


