# From "Introduction to Algorithms"

Adj = {'s' : ['a','x'], 
       'a' : ['s','z'],
       'z' : ['a'],
       'x' : ['s','d','c'],
       'd' : ['x','c','f'],
       'c' : ['x','d','f'],
       'f' : ['d','c','v'],
       'v' : ['c','f']}

s = 's'

V = ['s','a','z','x','d','c','f','v']

# Breadth First Search
# From video lecture
#
def BFS1(s,Adj):
    level = {s : 0}
    parent = {s : None}
    i = 1
    frontier = [s]
    while frontier:
        next = []
        for u in frontier:
            for v in Adj[u]:
                if v not in level:
                    level[v] = i
                    parent[v] = u
                    next.append(v)
                    print(v)

        frontier = next
        i = i + 1

# Depth First Search 'visit'
# Will explore all nodes reachable
# from the starting node
# From video lecture
#
def DFSVisit(s,Adj):
    for v in Adj[s]:
        if v not in parent:
            parent[v] = s
            DFS1(s,Adj)
            print(v)

# Depth First Search
# From video lecture
#
def DFS1(V,Adj):
    parent = {}
    for s in V:
        if s not in parent:
            parent[s] = None
            DFSVisit(s,Adj)


# DFS non-recursive
# 
#

def minus(q,r):
    a = set(q)
    b = set(r)
    return list(a - b)

def plus(q,r):
    a = q + r
    return list(set(a))

def getrem(rem,adj,vis,cur):
    a = plus(rem,adj)
    b = minus(a,vis)
    return minus(b,cur)

def DFS2(s,Adj):
    visited = []
    remaining = [s]
    while remaining:
        current = remaining.pop()
        visited.append(current)
        remaining = getrem(remaining,Adj[current],visited,current)
        print current
        #print "adjacent ", Adj[current] 
        #print "visited ", visited
        #print "remaining ", remaining
    
    
parent = {s : None}

#DFSVisit(s,Adj)
DFS1(V,Adj)
print "***********"
DFS2(s,Adj)





