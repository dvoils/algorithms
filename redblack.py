import random

class Node:
    def __init__(self, key, color, p, left, right):
        self.key = key
        self.p = p
        self.color = color
        self.left = left
        self.right = right

class Tree:
    def __init__(self, leaf):
        self.nill = leaf
        self.root= self.nill
        
    def left_rotate(self,x):
        y = x.right
        x.right = y.left
        if y.left != self.nill:
            y.left.p = x
        y.p = x.p
        if x.p == self.nill:
            self.root = y
        elif x == x.p.left:
            x.p.left = y
        else: 
            x.p.right = y
        y.left = x
        x.p = y

    def right_rotate(self,y):
        x = y.left
        y.left = x.right
        if x.right != self.nill:
            x.right.p = y
        x.p = y.p
        if y.p == self.nill:
            self.root = x
        elif y == y.p.right:
            y.p.right = x
        else: 
            y.p.left = x
        x.right = y
        y.p = x

    def fixup(self,z):
        while z.p.color == "red":
            if z.p == z.p.p.left:
                y = z.p.p.right
                if y.color == "red":
                    z.p.color = "black"
                    y.color = "black"
                    z.p.p.color = "red"
                    z = z.p.p
                else:
                    if z == z.p.right:
                        z = z.p
                        self.left_rotate(z)
                    z.p.color = "black"
                    z.p.p.color = "red"
                    self.right_rotate(z.p.p)
            else:
                y = z.p.p.left
                if y.color == "red":
                    z.p.color = "black"
                    y.color = "black"
                    z.p.p.color = "red"
                    z = z.p.p
                else:
                    if z == z.p.left:
                        z = z.p
                        self.right_rotate(z)
                    z.p.color = "black"
                    z.p.p.color = "red"
                    self.left_rotate(z.p.p)
        self.root.color = "black"

    def insert(self,z):
        y = self.nill
        x = self.root
        while x != self.nill:
            y = x
            if z.key < x.key:
                x = x.left
            else:
                x = x.right
        z.p = y
        if y == self.nill:
            self.root = z
        elif z.key < y.key:
            y.left = z
        else:
            y.right = z
        z.left = self.nill
        z.right = self.nill
        z.color = "red"
        self.fixup(z)

    def search(self,k):
        x = self.root
        while x != None and k != x.key:
            if k < x.key:
                print "left", x.key
                x = x.left
            else:
                print "right", x.key
                x = x.right
        return x

N = 2000

a = random.sample(xrange(1,N*2), N)
leaf = Node(None, "", None, None, None)
rbTree = Tree(leaf)

for i in range(1,N):
    z = Node(a[i], "", None, None, None)
    rbTree.insert(z)

q = rbTree.search(a[5])
print a[5], q.key



