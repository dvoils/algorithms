import random

# Binary search tree example
# From Introduction to Algorithms text book
#

class Node:
    def __init__(self, key, p, left, right):
        self.key = key
        self.p = p
        self.left = left
        self.right = right

# Insert new node into the tree
#
def insert(r,z):
    y = None
    x = r
    while x != None:
        y = x
        if z.key < x.key:
            x = x.left
        else:
            x = x.right
    z.p = y
    if y == None:
        r = z
    elif z.key < y.key:
        y.left = z
    else:
        y.right = z

# Search tree for a key value
# Using iteration
#
def iter_search(x,k):
    while x != None and k != x.key:
        if k < x.key:
            print "left", x.key
            x = x.left
        else:
            print "right", x.key
            x = x.right
    return x

N = 200

a = random.sample(xrange(1,N*2), N)
r = Node(a[0], None, None, None)

for i in range(1,N):
    z = Node(a[i], None, None, None)
    insert(r,z)

q = iter_search(r,a[5])
print a[5], q.key
        
